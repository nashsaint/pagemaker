<?php

namespace App\Controller\Admin;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Entity\Post;
use App\Entity\Page;

class BaseController extends Controller
{
    protected $em;
    protected $user;

    public function __construct(ContainerInterface $container)
    {
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    /**
     * slugify name 
     * @param  [type] $text [description]
     * @param  [type] $ent  [description]
     * @return [type]       [description]
     */
    public function slugify($text, $ent)
    {
    	$slug 			= strtolower(str_replace(' ', '_', $text));
    	$uniqueSlug		= false;

    	while (!$uniqueSlug) {
    		if (!$dbSlug = $this->em->getRepository("\\App\\Entity\\$ent")->findOneBySlug($slug)) {
    			$uniqueSlug = true;
    		} else {
    			$slug .= mt_rand(1,100);
    		}
    	}
    	
    	return $slug;
    }
}
