<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use App\Entity\Post;
use App\Form\PostType;
use App\Controller\Admin\BaseController;

class PostController extends BaseController
{
    /**
     * @Route("/admin/post", name="admin_post_index")
     */
    public function index(Request $request)
    {
    	$qb = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App\Entity\Post', 'p')
        	->orderBy('p.name', 'asc')
        ;

        $adapter = new DoctrineORMAdapter($qb);
        $pager   = new Pagerfanta($adapter);

        $pager->setMaxPerPage(25);
        if ($page = $request->query->get('page')) {
            $pager->setCurrentPage($page);
        }

        return $this->render('admin/post/index.html.twig', [
        	'pager'		=> $pager,
        ]);
    }

    /**
     * @Route("/admin/post/new", name="admin_post_new",
     *     defaults={
     *         "parent" = "post"
     *     }
     * )
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function new(Request $request)
    {
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $slug = $this->slugify($post->getName(), 'Page');
            $post->setSlug($slug);

            $this->em->persist($post);
            $this->em->flush();

            $this->addFlash('success', sprintf("Page <b>%s</b> has been successfully saved.", $post->getName()));

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('admin/post/form.html.twig', [
            'form'      => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/post/{slug}/edit", name="admin_post_edit",
     *     defaults={
     *         "parent" = "post"
     *     }
     * )
     * 
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function edit(Request $request, $slug)
    {
        if (!$post = $this->em->getRepository(Post::class)->findOneBySlug($slug)) {
            throw new NotFoundHttpException(sprintf("Post %s Not Found!", $slug));
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $post->setStatus($request->request->get('status'));

            $this->em->persist($post);
            $this->em->flush();

            $this->addFlash('success', sprintf("Changes to post <b>%s</b> has been successfully saved.", $post->getName()));

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('admin/post/form.html.twig', [
            'form'  => $form->createView(),
        ]);
    }
}
