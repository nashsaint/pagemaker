<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use App\Entity\Page;
use App\Form\PageType;

use App\Controller\Admin\BaseController;

class PageController extends BaseController
{
    /**
     * @Route("/admin/page", name="admin_page_index")
     */
    public function index(Request $request)
    {

        $qb = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App\Entity\Page', 'p')
        ;
        
        if (!null == $request->query->get('status')) {
            $qb->where('p.status = :status')
               ->setParameter('status', $request->query->get('status'));
        }

        $qb->orderBy('p.name', 'asc');

        $adapter = new DoctrineORMAdapter($qb);
        $pager   = new Pagerfanta($adapter);

        $pager->setMaxPerPage(25);
        if ($page = $request->query->get('page')) {
            $pager->setCurrentPage($page);
        }

        return $this->render('admin/page/index.html.twig', [
    		'pager'	=> $pager,
        ]);
    }

    /**
     * @Route("/admin/page/new", name="admin_page_new",
     *     defaults={
     *         "parent" = "page"
     *     }
     * )
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function new(Request $request)
    {
        $page = new Page();

        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $slug = $this->slugify($page->getName(), 'Page');
            $page->setSlug($slug);

            $this->em->persist($page);
            $this->em->flush();

            $this->addFlash('success', sprintf("Page <b>%s</b> has been successfully saved.", $page->getName()));

            return $this->redirectToRoute('admin_page_index');
        }

        return $this->render('admin/page/form.html.twig', [
            'form'      => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/page/{slug}/edit", name="admin_page_edit",
     *     defaults={
     *         "parent" = "page"
     *     }
     * )
     * 
     * @param  [type] $slug [description]
     * @return [type]       [description]
     */
    public function edit(Request $request, $slug)
    {
        if (!$page = $this->em->getRepository(Page::class)->findOneBySlug($slug)) {
            throw new NotFoundHttpException(sprintf("Page %s Not Found!", $slug));
        }

        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $page->setStatus($request->request->get('status'));

            $this->em->persist($page);
            $this->em->flush();

            $this->addFlash('success', sprintf("Changes to page <b>%s</b> has been successfully saved.", $page->getName()));

            return $this->redirectToRoute('admin_page_index');
        }

        return $this->render('admin/page/form.html.twig', [
            'form'  => $form->createView(),
        ]);
    }
}
