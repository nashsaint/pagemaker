<?php

namespace App\Controller\Admin;

use App\Entity\Role;
use App\Form\RoleType;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use App\Controller\Admin\BaseController;

class SettingController extends BaseController
{
    /**
     * @Route("/admin/setting", name="admin_setting_index")
     */
    public function index(Request $request)
    {
    	$role 		= new Role();
    	$roleForm 	= $this->createForm(RoleType::class, $role);

    	$roleForm->handleRequest($request);

    	$roles 		= $this->em->getRepository(Role::class)->findAll();

    	if ($roleForm->isSubmitted() && $roleForm->isValid()) {
    		$this->em->persist($role);
    		$this->em->flush();

    		$this->addFlash('success', sprintf("Role <b>%s</b> has been added!", $role->getName()));
    	}

        return $this->render('admin/setting/index.html.twig', [
         	'roleForm'	=> $roleForm->createView(),
         	'roles'		=> $roles,
        ]);
    }
}
