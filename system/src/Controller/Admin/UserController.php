<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use App\Controller\Admin\BaseController;
use App\Entity\User;
use App\Form\UserType;

class UserController extends BaseController
{
    /**
     * @Route("/admin/user", name="admin_user_index")
     */
    public function index(Request $request)
    {
    	$qb = $this->em->createQueryBuilder()
            ->select('p')
            ->from('App\Entity\User', 'p')
            ->orderBy('p.firstname', 'asc')
        ;

        $adapter = new DoctrineORMAdapter($qb);
        $pager   = new Pagerfanta($adapter);

        $pager->setMaxPerPage(25);
        if ($page = $request->query->get('page')) {
            $pager->setCurrentPage($page);
        }

        return $this->render('admin/user/index.html.twig', [
            'pager'		=> $pager,
        ]);
    }

    /**
     * @Route("/admin/user/{username}/edit", name="admin_user_edit",
     *     defaults={
     *         "parent" = "user"
     *     }
     * )
     * 
     * @param  [type] $username [description]
     * @return [type]       [description]
     */
    public function edit(Request $request, $username)
    {
        if (!$user = $this->em->getRepository(User::class)->findOneByUsername($username)) {
            throw new NotFoundHttpException(sprintf("User %s Not Found!", $username));
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash('success', sprintf("Changes to user <b>%s</b> has been successfully saved.", $user->getFirstname()));

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/form.html.twig', [
            'form'  => $form->createView(),
        ]);
    }
}
