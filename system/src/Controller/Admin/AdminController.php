<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Controller\Admin\BaseController;
use App\Entity\Page;
use App\Entity\Post;

class AdminController extends BaseController
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function index()
    {
    	$pages = [
    		'published'		=> $this->em->getRepository(Page::class)->findByStatus(Page::STATUS_PUBLISHED),
    		'draft' 		=> $this->em->getRepository(Page::class)->findByStatus(Page::STATUS_DRAFT),
    	];
    	$posts = [
    		'published'		=> $this->em->getRepository(Post::class)->findByStatus(Post::STATUS_PUBLISHED),
    		'draft' 		=> $this->em->getRepository(Post::class)->findByStatus(Post::STATUS_DRAFT),
    	];

        return $this->render('admin/dashboard.html.twig', [
        	'pages'		=> $pages,
        	'posts'		=> $posts,
        ]);
    }
}
