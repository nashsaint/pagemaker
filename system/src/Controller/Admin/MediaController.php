<?php

namespace App\Controller\Admin;

use App\Entity\Media;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use App\Controller\Admin\BaseController;

class MediaController extends BaseController
{
    /**
     * @Route("/admin/media", name="admin_media_index")
     */
    public function index()
    {
    	$medias = $this->em->getRepository(Media::class)->findAll();

        return $this->render('admin/media/index.html.twig', [
       		'medias' 	=> $medias,
        ]);
    }

    /**
     * upload media 
     *
     * @Route("/admin/media/upload", name="admin_media_upload")
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function upload(Request $request)
    {
    	$uploadPath  = $this->getParameter('upload');
    	$path 		 = $uploadPath['path'];
    	$uploadDir 	 = $uploadPath['dir'];
    	$standardSize= $uploadPath['size']['standard'];
    	$thumbSize	 = $uploadPath['size']['thumb'];
    	$quality 	 = $uploadPath['quality'];

    	$files = $request->files->get('files');

    	foreach ($files as $file) {
    		$originalName = $file->getClientOriginalName();

    		$ext 		= $file->getClientOriginalExtension();
    		$filename 	= md5(uniqid(rand(), true)) . ".$ext";

    		$dir     	= date('Y');
    		$thumb 		= '/thumb/';

    		$fs 		= new Filesystem();
    		try {
    			$fs->mkdir($path . $dir);
    			$fs->mkdir($path . $dir . $thumb);
    		} catch (IOExceptionInterface $e) {
    			echo "An error occured while creating a directory at " . $exception->getPath();
    		}
    		
    		// save original size
    		$image 		= Image::make($file);
    		$image->save($path . $dir . '/' . $filename, $quality);
    		$filesize 	  = $image->filesize();

    		// save thumbnail
    		$image->resize($thumbSize, null, function($constraint) {
    			$constraint->aspectRatio();
    		});
    		$image->save($path . $dir . $thumb . $filename, $quality);

    		// persist media
    		$media = new Media();
    		$media->setOriginalName($originalName)
    			  ->setFilename($filename)
    			  ->setSize($filesize)
    			  ->setExt($ext)
    			  ->setLocation($uploadDir . $dir . '/')
    		;

    		$this->em->persist($media);
    	}

		if ($files) {
			$this->em->flush();
		}

		return $this->redirectToRoute('admin_media_index');
    }
}
