<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    const STATUS_DRAFT      = 1;
    const STATUS_PUBLISHED  = 2;

    public function statusNames()
    {
        return [
            self::STATUS_DRAFT      => 'Draft',
            self::STATUS_PUBLISHED  => 'Published',
        ];
    }

    public function statusName($id)
    {
        if (array_key_exists($id, $this->statusNames())) {
            return $this->statusNames()[$id];
        }

        return null;
    }

    public function statusColours()
    {
        return [
            self::STATUS_DRAFT      => 'gray',
            self::STATUS_PUBLISHED  => 'green'
        ];
    }

    public function statusColour($id)
    {
        if (array_key_exists($id, $this->statusColours())) {
            return $this->statusColours()[$id];
        }

        return null;
    }
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=90, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", name="content", nullable=true)
     */
    protected $content;

    /**
     * @ORM\Column(type="integer", name="status", nullable=true)
     */
    protected $status = self::STATUS_DRAFT;

    /**
     * @ORM\Column(type="string", name="slug", length=90, nullable=true)
     */
    protected $slug;
    
    /**
     * @ORM\Column(type="datetime", name="created_at", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    protected $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     *
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
}
