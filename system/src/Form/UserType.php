<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'attr'              => [
                    'placeholder'   => 'Username',
                    'class'         => 'form-control'
                ],
                'label_attr'        => [
                    'class'         => 'form-label'
                ]
            ])
            ->add('firstname', TextType::class, [
                'attr'              => [
                    'placeholder'   => 'Firstname',
                    'class'         => 'form-control'
                ],
                'label_attr'        => [
                    'class'         => 'form-label'
                ]
            ])
            ->add('lastname', TextType::class, [
                'attr'              => [
                    'placeholder'   => 'Lastname',
                    'class'         => 'form-control'
                ],
                'label_attr'        => [
                    'class'         => 'form-label'
                ]
            ])
            ->add('email', EmailType::class, [
                'label'             => 'Email Address',
                'attr'              => [
                    'placeholder'   => 'email@address.com',
                    'class'         => 'form-control'
                ],
                'label_attr'        => [
                    'class'         => 'form-label'
                ]
            ])
            ->add('roles', EntityType::class, [
                'class'             => Role::class, 
                'choice_label'      => 'name',
                'attr'              => [
                    'class'         => 'form-control'
                ],
                'query_builder'     => function(EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->orderBy('r.name', 'ASC');
                }
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type'              => PasswordType::class,
                'required'          => false,
                'first_options'     => [
                    'label'         => 'Password',
                    'attr'              => [
                        'placeholder'   => 'Username',
                        'class'         => 'form-control'
                    ],
                    'label_attr'        => [
                        'class'         => 'form-label'
                    ]
                ],
                'second_options'    => [
                    'label'         => 'Repeat Password',
                    'attr'              => [
                        'placeholder'   => 'Username',
                        'class'         => 'form-control'
                    ],
                    'label_attr'        => [
                        'class'         => 'form-label'
                    ]
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
