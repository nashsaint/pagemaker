<?php

namespace App\Form;

use App\Entity\Page;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr'          => [
                    'class'     => 'form-control'
                ],
                'label_attr'    => [
                    'class'     => 'form-label'
                ]
            ])
            ->add('content', TextareaType::class, [
                'required'      => false,
                'attr'          => [
                    'class'     => 'form-control trumbowyg',
                    'rows'      => 10
                ],
                'label_attr'    => [
                    'class'     => 'form-label'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
