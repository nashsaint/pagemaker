<?php

namespace App\Form;

use App\Entity\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr'              => [
                    'placeholder'   => 'Administrator',
                    'class'         => 'form-control'
                ],
                'label_attr'        => [
                    'class'         => 'form-label'
                ]
            ])
            ->add('role', TextType::class, [
                'attr'              => [
                    'placeholder'   => 'ROLE_USER',
                    'class'         => 'form-control upper'
                ],
                'label_attr'        => [
                    'class'         => 'form-label'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Role::class,
        ]);
    }
}
