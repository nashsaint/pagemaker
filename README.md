## What
This is a skeleton for building websites using symfony framework v 4.1.2 (This version is pretty fast!);

## Framework & Modules
* [Symfony 4.1](https://symfony.com/)
* npm
* gulp

## CSS Framework

**Spectre**

Using [Semantic UI](https://picturepan2.github.io/spectre/) as the main component library. Why recreate the wheel.  This is a very light framework.

**Notes**

Do not edit anything inside `public/build/`, use `public/dev/` instead.  

Check `public/gulpfile.js`.

run a gulp task to generate the build assets.  Simpliest way is to just run `gulp watch` inside `httpdocs` and let gulp do the magic.

There is a specific `.js` and `.less` files for each board in `httpdocs/build/dev/`

## DB Connections
Check `system/config/packages/doctrine.yaml` for db connections.