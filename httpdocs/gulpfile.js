// gulp modules
var gulp 	= require('gulp'),
	less 	= require('gulp-less'),
	cssmin 	= require('gulp-cssmin'),
	concat	= require('gulp-concat'),
	uglify  = require('gulp-uglify')
;

// asset dirs
var DEV_ADMIN_DIR   		= 'dev/admin/',
	DEV_ADMIN_LESS_DIR		= DEV_ADMIN_DIR + 'less/',
	DEV_ADMIN_JS_DIR 		= DEV_ADMIN_DIR + 'js/',

	BUILD_ADMIN_DIR 		= 'build/admin/',
	BUILD_ADMIN_CSS_DIR 	= BUILD_ADMIN_DIR + 'css/'
	BUILD_ADMIN_JS_DIR 		= BUILD_ADMIN_DIR + 'js/'
;

// tasks
gulp.task('less', function() {
    return gulp.src(DEV_ADMIN_LESS_DIR + 'app.less',)
        .pipe(less())
        .pipe(cssmin())
        .pipe(concat('admin-app.min.css'))
        .pipe(gulp.dest(BUILD_ADMIN_CSS_DIR));
});

gulp.task('js', function() {
    return gulp.src(
        [DEV_ADMIN_JS_DIR + 'app.js'])
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest(BUILD_ADMIN_JS_DIR));
});

// watch 
gulp.task('watch', function() {
    gulp.watch([DEV_ADMIN_LESS_DIR + 'app.less'], ['less']);
});